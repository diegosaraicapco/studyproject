package br.com.capco.controller;

import br.com.capco.config.Config;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MainExample{

    public static void main(String[] args){
        SpringApplication.run(Config.class, args);
    }
}
