package br.com.capco.controller;

import br.com.capco.entity.ClientEntity;
import br.com.capco.dto.ExampleResponseDTO;
import br.com.capco.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class MainController {

    @Autowired
    private ClientService clientService;

     @RequestMapping(value="/test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ExampleResponseDTO testGet(){

        Optional<ClientEntity> clientOptional = clientService.getClient();

        if(clientOptional.isPresent()){

            ClientEntity client = clientOptional.get();

            ExampleResponseDTO d = new ExampleResponseDTO();
            d.setNome(client.getName());

            return d;
        }

        return null;
    }
}
