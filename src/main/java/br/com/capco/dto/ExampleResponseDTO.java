package br.com.capco.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

@ApiModel("ExampleRequestDTO")
public class ExampleResponseDTO {

    @JsonProperty("nome")
    @ApiModelProperty(name="nome")
    private String nome;

    @JsonProperty("idade")
    @ApiModelProperty(name="idade")
    private Integer idade;

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public Integer getIdade() {
        return idade;
    }
    public void setIdade(Integer idade) {
        this.idade = idade;
    }
}
