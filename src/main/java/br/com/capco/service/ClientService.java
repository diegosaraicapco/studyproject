package br.com.capco.service;

import br.com.capco.entity.ClientEntity;
import br.com.capco.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public Optional<ClientEntity> getClient(){

        return clientRepository.findById(1L);
    }

}
